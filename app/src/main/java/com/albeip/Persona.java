package com.albeip;

import java.util.Date;

public class Persona {
    private final String nombre;
    private final char sexo;
    private final Date fechaNacimiento;
    private final boolean tieneComorbilidad;

    public boolean isTieneComorbilidad() {
        return tieneComorbilidad;
    }

    public Long getEdad() {
        Long fechaNacimientoEnMilisegundos = fechaNacimiento.getTime();
        Long fechaActualEnMillisegundos    = (new Date()).getTime();
        return (fechaActualEnMillisegundos - fechaNacimientoEnMilisegundos)/Util.ANIO_EN_MILISEGUNDOS;
    }

    public char getSexo() {
        return sexo;
    }


    public Persona(String nombre, char sexo, Date fechaNacimiento, boolean tieneComorbilidad) {
        this.nombre = nombre;
        this.sexo = sexo;
        this.fechaNacimiento = fechaNacimiento;
        this.tieneComorbilidad = tieneComorbilidad;
    }

    @Override
    public String toString() {
        return "Persona{" +
                "nombre='" + nombre + '\'' +
                ", sexo=" + sexo +
                ", fechaNacimiento=" + fechaNacimiento +
                ", tieneComorbilidad=" + tieneComorbilidad +
                '}';
    }
}
