package com.albeip;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Util {
    public static final Long ANIO_EN_MILISEGUNDOS = 31556900000L;

    public static void limpiaPantalla() {
        System.out.print("\033\143");
    }

    public static void espera() throws InterruptedException {
        Thread.sleep(2 * 1000); // Espera 2 segundos
    }

    public static Date convierteAFecha(String string) throws ParseException {
        SimpleDateFormat formmatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
        return formmatter.parse(string);
    }

    public static boolean esSi(String string) {
        return string!=null && string.toLowerCase().equals("si");
    }

}
